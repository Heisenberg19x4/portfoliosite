import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import './project_panel.css'

//________________________PROJECT_____________________
import { inventorBuild1, inventorBuild2,inventorBuild3,inventorGroup, inventorPanel1,inventorPanel2,canWindow1,
  canWindow2,robotControl2,robotControl3,tractorRobot2,dogRobot1} from '../../img/project_img';
import { Plata1, mapTablet1,tractorRobot1,robotControl1,dogRobot2} from '../../img/project_img';


//________________________LOGO_____________________
import people1 from '../../img/people/ekaterina-sankova2.svg';
import people2 from '../../img/people/viktor_tsvetkov2.svg';

export default function ProjectPanel() {
  useEffect(() => {
    const projectFeedElement = document.querySelector('.Project-feed-element1');
    const images = projectFeedElement.querySelectorAll('img');
    const cloneImages = [...images].map(img => img.cloneNode(true));
    cloneImages.forEach(img => projectFeedElement.appendChild(img));
  }, []);
  useEffect(() => {
    const projectFeedElement = document.querySelector('.Project-feed-element2');
    const images = projectFeedElement.querySelectorAll('img');
    const cloneImages = [...images].map(img => img.cloneNode(true));
    cloneImages.forEach(img => projectFeedElement.appendChild(img));
  }, []);

    return(
        <div className='Project'>
          <div className='Project-grid'>
            <div className='Project-feed'>
              <div className='Project-feed-element-logo'>
                <Link to="/projects/ekaterina_sankova">
                  <img src={people1} alt="Logo" />
                </Link>
              </div>
            </div>
            <div className='Project-feed'>
              <div className='Project-feed-element1'>
                <img src={dogRobot1} alt="Porfolio"/>
                <img src={canWindow1} alt="Porfolio"/>
                <img src={canWindow2} alt="Porfolio"/>
                <img src={robotControl2} alt="Porfolio"/>
                <img src={robotControl3} alt="Porfolio"/>
                <img src={tractorRobot2} alt="Porfolio"/>
                <img src={inventorBuild1} alt="Porfolio"/>
                <img src={inventorPanel1} alt="Porfolio"/>
                <img src={inventorPanel2} alt="Porfolio"/>
              </div>
            </div>
            <div className='Project-feed'>
              <div className='Project-feed-element-logo'>
                <Link to="/projects/viktor_tsvetkov">
                  <img src={people2} alt="Logo" />
                </Link>
              </div>
            </div>
            <div className='Project-feed'>
              <div className='Project-feed-element2'>
                <img src={dogRobot2} alt="Porfolio"/>
                <img src={Plata1} alt="Porfolio"/>
                <img src={mapTablet1} alt="Porfolio"/>
                <img src={tractorRobot1} alt="Porfolio"/>
                <img src={robotControl1} alt="Porfolio"/>
              </div>
            </div>
          </div>
        </div>
    );
}
