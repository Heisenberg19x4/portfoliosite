import React from 'react';
import "./project_card.css";

const ProjectCard = ({ title, image, description,phone,email,name }) => (
  <div className="project_card-container">
    <div className='project_card-grid'>
        <div project_card-left_column>
            <div className='project_card-header'>
                    {title}
            </div>
            <div className='project_card-logo'>
                <img src={image} alt="Null" width={"225px"} height={"248px"}/>
            </div>
            <div className='project_card-contact-grid'>
                <div className='project_card-item'>{name}</div>
                <div className='project_card-item'>{phone}</div>
                <div className='project_card-item'>{email}</div>

            </div>
        </div>

        <div className='project_card-description'>
            {description}
        </div>
    </div>
  </div>
)

export default ProjectCard 