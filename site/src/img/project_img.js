
export { default as inventorBuild1 } from  './projects/api_inventor/build1_project1.svg';
export { default as inventorBuild2  } from './projects/api_inventor/build2_project1.svg';
export { default as inventorBuild3  } from './projects/api_inventor/build3_project1.svg';
export { default as inventorGroup  } from './projects/api_inventor/group_deteils_project1.svg';
export { default as inventorPanel1  } from './projects/api_inventor/panel_project1.svg';
export { default as inventorPanel2  } from './projects/api_inventor/panel2_project1.svg';
export { default as Plata1  } from './projects/board/plata_project5.svg';
export { default as canWindow2  } from './projects/can/window1_project6.svg';
export { default as canWindow1  } from './projects/can/window2_project6.svg';
export { default as mapTablet1  } from './projects/map/tablet1_project3.svg';
export { default as robotControl1  } from './projects/robot_control_block/tablet1_project2.svg';
export { default as robotControl2  } from './projects/robot_control_block/tablet2_project2.svg';
export { default as robotControl3  } from './projects/robot_control_block/tablet3_project2.svg';
export { default as tractorRobot1  } from './projects/tractor/robot1_project4.svg';
export { default as tractorRobot2 } from'./projects/tractor/robot2_project4.svg';
export { default as dogRobot1  } from './projects/dog/robot1_project8.svg';
export { default as dogRobot2 } from'./projects/dog/robot2_project8.svg';

export { default as canCard  } from './projects/can_card.svg';
export { default as inventorapiCard } from'./projects/inventorapi_card.svg';
export { default as robotcontrolCard  } from './projects/robotcontrol_card.svg';
export { default as tractorCard } from'./projects/tractor_card.svg';
export { default as faceID } from'./projects/faceid.gif';