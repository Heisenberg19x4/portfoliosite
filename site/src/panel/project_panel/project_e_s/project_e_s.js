import React from 'react';
import './project_e_s.css'

//________________________LOGO_____________________
import people1 from '../../../img/people/ekaterina-sankova2.svg';

import { canCard, inventorapiCard,robotcontrolCard,tractorCard, faceID} from '../../../img/project_img';
import { logo_github,logo_gitlab} from '../../../img/contacts_icons';

export default function ProjectPanelES() {
    return(
        <div className='ProjectES'>
          <div className='ProjectES-grid'>
            <div className='ProjectES-header-grid'>
              <div className='ProjectES-header-grid-logo'>
                <img src={people1} alt="Logo" />
              </div>
            </div>
            <div className='ProjectES-body-grid'>
              <div className='ProjectES-body-grid-row0'>
                <img src={faceID} alt="faceID" height="500px" />
                Developed a real-time face recognition system <br /><br />
                on 20 cameras. <br /><br />
                Develop and deployed on a server.<br /><br /><br />
                STACK: Docker, Minio, Python,<br /> 
                Deep Face, OpenCV.
              </div>
              <div className='ProjectES-body-grid-row1'>
                <div className='ProjectES-body-grid-item-left'>
                  <img src={robotcontrolCard} alt="robotcontrolCard" />
                  Developed architecture : MVVM, microservices.<br /><br />
                  Production.<br /><br />
                  project for autonomous loading/unloading drone delivery blocks<br />
                  Backend + participation in frontend.<br /><br /><br />
                  STACK: C++17, QT6, MongoDB, Go, C#, <br />
                  Docker, RabbitMQ, Redis,<br />
                  Google Test.
                </div>
                <div className='ProjectES-body-grid-item-right'>
                  <img src={tractorCard} alt="tractorCard" />
                  Prototyped an autonomous robot. Programmed microcontrollers.<br /><br />
                  Route planning, autonomous robot<br /><br />
                  Movement algorithm, motor step calculation, motion sensors. <br />
                  Sensor calibration.<br /><br />
                  Deployed microservices architecture (server + clients).<br /><br />
                  Developed a CAN bus configurator (backend + frontend).<br /><br /><br />
                  STACK: C++17, C#, QT6, ESP32, Raspberry Pi, CAN
                </div>
              </div>
              <div className='ProjectES-body-grid-row2'>
                <img src={canCard} alt="canCard" />
                  Development of the can protocol <br /><br />
                  for communication of robot modules.<br /><br />
                  Node configuration interface<br /><br /><br />
                  STACK: C++20, QT6
              </div>
              <div className='ProjectES-body-grid-row3'>
              <img src={inventorapiCard} alt="inventorapiCard" />
                Engineering project<br /><br /> 
                using C++ with<br /><br />
                Autodesk Inventor API<br />
              </div>
              <div className='ProjectES-body-grid-row4'>
              <img src={logo_gitlab} alt="logo_gitlab" />
              https://gitlab.com/Heisenberg19x4
              </div>
            </div>
          </div>
        </div>
    )
}
/*
<div className='ProjectES-header-grid'>
          <div className='ProjectES-header-grid-logo'>
              <img src={people1} alt="Logo" />
          </div>
          <div className='ProjectES-header-grid-content'>
              yo
          </div>
          </div>
          <div className='ProjectES-body'>
            <div className='ProjectES-body-grid'>
            matherfucker
            </div>
          </div>
*/