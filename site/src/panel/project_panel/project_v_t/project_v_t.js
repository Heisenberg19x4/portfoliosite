
import React from 'react';
import './project_v_t.css'

//________________________LOGO_____________________
import people1 from '../../../img/people/viktor_tsvetkov2.svg';

import { canCard, inventorapiCard,robotcontrolCard,tractorCard, faceID} from '../../../img/project_img';
import { logo_github,logo_gitlab} from '../../../img/contacts_icons';
import tracktor from '../../../img/projects/covers/tracktor_vt_cover.png';
import truck from '../../../img/projects/covers/truck_cover.png';
import webdeveloper from '../../../img/projects/covers/webdeveloper_cover.png';
import roboto from '../../../img/projects/covers/roboto_cover.png';
import smart_home from '../../../img/projects/covers/smart_home_cover.png';


export default function ProjectPanelES() {
    return(
        <div className='ProjectES'>
          <div className='ProjectES-grid'>
            <div className='ProjectES-header-grid'>
              <div className='ProjectES-header-grid-logo'>
                <img src={people1} alt="Logo" />
              </div>
            </div>
            <div className='ProjectES-body-grid'>
              <div className='ProjectES-body-grid-row0'>
                <img src={tracktor} alt="faceID" height="500px" width="650px" />
                Was a team lead of 20 people as it-dep <br /><br />
                Designed architecture of ECU module <br /><br />
                Developed a long range radio module for drones<br /><br /><br />
                Designed architeture for AI driven tracktor<br /> 
                C, Kicad, SolidWorks,enterprise architect
              </div>
              <div className='ProjectES-body-grid-row1'>
                <div className='ProjectES-body-grid-item-left'>
                <img src={truck} alt="truck" height="500px" width="550px" />
                  Was a tech lead of a small team<br /><br />
                  Designed architecture of launcher app <br /><br />
                  Designed firmware for AI driven truck for mining operations<br />
                  Was a lead developer of a firmware<br /><br /><br />
                  STACK: C++17, QT5,sqlite <br />
                  QML<br />
                  Google Test.
                </div>
                <div className='ProjectES-body-grid-item-right'>
                  <img src={smart_home} alt="tractorCard" width="300px"/>
                  Was a CTO in Smart home project.<br /><br />
                  Planning it-department tasks<br /><br />
                  Did teck solutions for whole project <br />
                  System analitic<br /><br />
                  Architecture design<br /><br />
                  Product managment control<br /><br /><br />
                  STACK: C, freertos, TI ,rpi, PHP , openwrt,enterprise architect,jira,bitrix
                </div>
              </div>
              <div className='ProjectES-body-grid-row2'>
                <img src={roboto} alt="canCard" width="800px"/>
                  Was an embedded developer on a medtech <br /><br />
                  Wrote code for rehabilitation robot<br /><br />
                  Coded DSP MCU in Assembly<br /><br /><br />
                  Wrote code in C# for desktop<br /><br /><br />
                  Did data engeniring for neural nets<br /><br /><br />
                  STACK: C-11, freertos,pytorch, python, pandas,eclips,C#,MVM,WPF
              </div>
              <div className='ProjectES-body-grid-row3'>
              <img src={webdeveloper} alt="inventorapiCard" width="650px" height="600px" />
                Long time was a full-stack full time webdev<br /><br /> 
                using django grpc golang <br /><br />
                Good knowledge in QML.React,Angular<br />
              </div>
              <div className='ProjectES-body-grid-row4'>
              <img src={logo_github} alt="logo_gitlab" />
              https://github.com/Mentalsupernova
              </div>
            </div>
          </div>
        </div>
    )
}

          // <div><a href="https://www.freepik.com/free-vector/bionics-icons-set_4005948.htm#query=robo%20rehabilitation&position=6&from_view=search&track=ais">Image by macrovector</a> on Freepik</div>
          // <div><a href="https://www.freepik.com/free-vector/bionics-icons-set_4005948.htm#query=robo%20rehabilitation&position=6&from_view=search&track=ais#position=6&query=robo%20rehabilitation">Image by macrovector</a> on Freepik</div>
          // <div><a href="https://www.freepik.com/free-vector/business-people-controlling-smart-house-devices-with-tablet-laptop-smart-home-devices-home-automation-system-domotics-market-concept-pinkish-coral-bluevector-isolated-illustration_11668238.htm#query=smart%20home&position=44&from_view=search&track=ais">Image by vectorjuice</a> on Freepik</div>
          // <div><a href="https://www.freepik.com/free-vector/internet-things-iot-smart-connection-control-device-network-industry-resident-anywhere-anytime-anybody-any-business-with-internet-it-technology-futuristic-world_25326198.htm#query=smart%20truck&position=1&from_view=search&track=ais">Image by jcomp</a> on Freepik</div>
          // <div><a href="https://www.freepik.com/free-vector/futuristic-technologies-farm_5901236.htm#query=smart%20tracktor&position=0&from_view=search&track=ais">Image by vectorpouch</a> on Freepik</div>
