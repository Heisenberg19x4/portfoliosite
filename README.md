# DESCRIPTION

The basic template of the portfolio site. 
Add your own images. See the paths to the main images in /src.

STACK: nodejs, react, Docker

![image info](./MainPictures.png)

# START

$ npm i
$ cd site/
$ npm run start