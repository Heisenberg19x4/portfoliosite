import React from 'react';
import './resume_v_t.css'

//________________________LOGO_____________________
import people1 from '../../../img/people/viktor_tsvetkov2.svg';
import { logo_hh} from '../../../img/contacts_icons';

//________________________SOFTWARE_____________________
import {kicad, kafka,rabbitMQ,nginx,docker,postgresql,mongodb,
  qt,python,android,rust,csharp,cplusplus,clang, solid,pytorch} from '../../../img/resume_icons';

export default function ResumePanelES() {

  return (
    <div className='ResumeES'>
      <div className="grid-container-header">
        <div className="grid-item">
          <img src={people1} alt="Logo" />
        </div>
        <div className="grid-item-header">
          Trust me  <br /><br />
          I'm an enginer 
        </div>
        <div className="grid-item-header-education">
          <div className="grid-item-header-education-title ">
            E d u c a t i o n
          </div>
          <div className="grid-item-header-education-item">
            Moscow Synergy University
          </div>
          <div className="grid-item-header-education-item">
            Bachalor degree in Computer science and information security
          </div>
          <div className="grid-item-header-education-item">
          September 2018 - May 2022
          </div>
        </div>
      </div>
        <div className="grid-container1">
          <div className="grid-item-since">
            figting  <br /><br />
            with cyborgs <br /><br />
            since <br /><br />
            2015
          </div>
          <div className="grid-item-software">
            <div className='grid-item-software-text'>
              S o f t w a r e
            </div>
            <div className='grid-item-software-icons'>
              <img src={cplusplus} alt="ResumeES"/>
              <img src={clang} alt="ResumeES"/>
              <img src={csharp} alt="ResumeES"/>
              <img src={rust} alt="ResumeES"/>
              <img src={android} alt="ResumeES"/>
              <img src={python} alt="ResumeES"/>
              <img src={qt} alt="ResumeES"/>
              <img src={mongodb} alt="ResumeES"/>
              <img src={postgresql} alt="ResumeES"/>
              <img src={nginx} alt="ResumeES"/>
              <img src={docker} alt="ResumeES"/>
              <img src={rabbitMQ} alt="ResumeES"/>
              <img src={kafka} alt="ResumeES"/>
              <img src={kicad} width="100px" height="100px" alt="ResumeES"/>
              <img src={solid} width="100px" height="100px" alt="ResumeES"/>
              <img src={pytorch} width="100px" height="100px" alt="ResumeES"/>
            </div>
          </div>
        </div>
      <div className="grid-container2">
        <div className="grid-container-experience-block">
          <div className="grid-container-experience-title">
          Experience
          </div>
          <div className="grid-container-experience-name">
            Team lead in Promavto <br/><br />
            Team lead in Piklema <br/><br />
            CTO in  hitepro <br/><br />
            Embedded software developer in IAI <br/> <br />
          </div>
        </div>
      </div>
      <div className='grid-container3'>
            <img src={logo_hh} alt="Logo" height="80" width="80" />
            https://hh.ru/resume/bba4bc94ff0bf8dcd30039ed1f4b5345783869
      </div>
    </div>
  );
}