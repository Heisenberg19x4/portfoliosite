export { default as logo_gmail } from  './contact/icons8-gmail-94 1.svg';
export { default as  logo_in} from  './contact/icons8-in-94.svg';
export { default as logo_tg } from  './contact/icons8-tg.svg';
export { default as logo_whatsapp } from  './contact/icons8-whatsapp.svg';
export { default as logo_hh } from  './contact/hh.svg';
export { default as logo_contact_whatsapp } from  './contact/contactswhatsapp.svg';

export { default as logo_gitlab } from  './contact/icons8-gitlab.svg';
export { default as logo_github } from  './contact/icons8-github.svg';