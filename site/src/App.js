import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';

import './App.css';

// _____________PANEL______________
import ProjectPanel from './panel/project_panel/project_panel';
import ContactsPanel from './panel/contacts_panel/contacts_panel';
import ResumePanel from './panel/resume_panel/resume_panel';
import MainPanel from './panel/main_panel/main_panel'

// _____________PERSON_PANEL______________
import ProjectPanelES from './panel/project_panel/project_e_s/project_e_s';
import ProjectPanelVT from './panel/project_panel/project_v_t/project_v_t';
import ResumePanelES from './panel/resume_panel/resume_e_s/resume_e_s';
import ResumePanelVT from './panel/resume_panel/resume_v_t/resume_v_t';


// _____________TEXT______________
import { MAIN_TEXT } from './text_const/header_text_const'; // Import the text constant from the separate file
import { PROJECTS_TEXT } from './text_const/header_text_const'; // Import the text constant from the separate file
import { RESUME_TEXT } from './text_const/header_text_const'; // Import the text constant from the separate file
import { CONTACTS_TEXT  } from './text_const/header_text_const'; // Import the text constant from the separate file


// _____________MAIN______________
function App() {
  const linkStyle = {
    textDecoration: 'none', // Remove the underline
    color: 'white'
  };

  return (
    <Router>
      <div className='App-header'>
        <div className='App-header-navigation'>
        <div className='App-header-link'>
            <Link to="/" style={linkStyle}>
              { MAIN_TEXT }
            </Link>
          </div>
          <div className='App-header-link'>
            <Link to="/projects" style={linkStyle}>
            { PROJECTS_TEXT }
            </Link>
          </div>
          <div className='App-header-link'>
            <Link to="/resume" style={linkStyle}>
            { RESUME_TEXT } 
            </Link>
          </div>
          <div className='App-header-link'>
            <Link to="/contacts" style={linkStyle}>
            { CONTACTS_TEXT  }
            </Link>
          </div>
        </div>
      </div>
      <Routes>
        <Route path="/" element={<MainPanel/>} />
        <Route path="/projects" element={<ProjectPanel/>} />           
        <Route path="/resume" element={<ResumePanel/>}/>           
        <Route path="/contacts" element={<ContactsPanel/>}/>  
        <Route path="/projects/ekaterina_sankova" element={<ProjectPanelES/>}/>           
        <Route path="/projects/viktor_tsvetkov" element={<ProjectPanelVT/>}/>
        <Route path="/resume/ekaterina_sankova" element={<ResumePanelES/>}/>           
        <Route path="/resume/viktor_tsvetkov" element={<ResumePanelVT/>}/>  
      </Routes>
    </Router>
  );
}


export default App;

