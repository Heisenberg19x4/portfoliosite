import React from 'react';
import './main_panel.css'

import KeyboardAnimation from '../animation/keyboard_animation';

export default function MainPanel() {
    return(
      <div className='Main'>
        <div className='Main-grid'>
        <div className='Main-banner'>
          <div className='Main-banner-text'>
            <KeyboardAnimation text=
              'Keep calm and hand over development to specialists'
              delay_write={100} delay_clear={800} />
          </div>
      </div>
      </div>
    </div>
    );
}
