import React, { useState, useEffect } from 'react';



const KeyboardAnimation = ({ text, delay_write, delay_clear}) => {
  const [currentText, setCurrentText] = useState('');
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    if (currentIndex < text.length) {
      const timeout = setTimeout(() => {
        setCurrentText(prevText => prevText + text[currentIndex]);
        setCurrentIndex(prevIndex => prevIndex + 1);
      }, delay_write);
  
      return () => clearTimeout(timeout);
    }
  }, [currentIndex, delay_write, text]);

  useEffect(() => {
    if (currentIndex === text.length) {
      const timeout = setTimeout(() => {
        setCurrentText('');
        setCurrentIndex(0);
      }, delay_clear);
  
      return () => clearTimeout(timeout);
    }
  }, [currentIndex, delay_clear, text]);
  return <div>{currentText}</div>;
};

export default KeyboardAnimation;