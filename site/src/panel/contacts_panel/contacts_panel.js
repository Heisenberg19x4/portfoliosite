import React from 'react';
import './contacts_panel.css'

import { logo_gmail, logo_tg,logo_in,logo_whatsapp,logo_hh,logo_contact_whatsapp} from '../../img/contacts_icons';

export default function ContactsPanel() {
    return(
        <div className='Contacts'>
          <div className='Contacts-grid'>
            <div className='Contacts-grid-block'>
            Contact us
            </div>
            <div className='Contacts-grid-block'>
                <img src={logo_tg} alt="Logo" height="80" width="80" />
                @Santsvet
            </div>
            <div className='Contacts-grid-block'>
                <img src={logo_gmail} alt="Logo" height="80" width="80"  />
                santsvetcompany@gmail.com
            </div>
            <div className='Contacts-grid-block'>
                <img src={logo_whatsapp} alt="Logo" height="80" width="80" />
                <img src={logo_contact_whatsapp} alt="Logo" />
            </div>
            
          </div>
        </div>
    );
}
