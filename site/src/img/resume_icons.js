
export { default as inventor } from  './software/icons8-inventor.svg';
export { default as solid  } from './software/icons8-solidworks.svg';
export { default as fusion  } from './software/autodesk_fusion.svg';
export { default as kafka  } from './software/kafka.svg';
export { default as rabbitMQ  } from './software/rabbitmq1.svg';
export { default as nginx  } from './software/icons8-nginx.svg';
export { default as docker  } from './software/icons8-docker.svg';
export { default as postgresql  } from './software/icons8-postgresql.svg';
export { default as mongodb  } from './software/icons8-mongodb.svg';
export { default as qt  } from './software/icons8-qt.svg';
export { default as python  } from './software/icons8-python.svg';
export { default as android  } from './software/icons8-android.svg';
export { default as rust  } from './software/icons8-rust.svg';
export { default as csharp  } from './software/icons8c-sharp.svg';
export { default as cplusplus } from'./software/icons8-c++-100.svg';
export { default as clang  } from './software/icons8-c.svg';
export { default as kicad  } from './software/icons9-kicad.png';
export { default as pytorch  } from './software/icons10-pytorch.svg';