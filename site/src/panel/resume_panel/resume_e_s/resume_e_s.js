import React from 'react';
import './resume_e_s.css'

//________________________LOGO_____________________
import people1 from '../../../img/people/ekaterina-sankova2.svg';
import { logo_in} from '../../../img/contacts_icons';

//________________________SOFTWARE_____________________
import { inventor,solid,fusion,kafka,rabbitMQ,nginx,docker,postgresql,mongodb,
  qt,python,android,rust,csharp,cplusplus,clang} from '../../../img/resume_icons';

export default function ResumePanelES() {

  return (
    <div className='ResumeES'>
      <div className="grid-container-header">
        <div className="grid-item">
          <img src={people1} alt="Logo" />
        </div>
        <div className="grid-item-header">
          I’m <br /><br />
          a  programmer  engineer 
        </div>
        <div className="grid-item-header-education">
          <div className="grid-item-header-education-title ">
            E d u c a t i o n
          </div>
          <div className="grid-item-header-education-item">
            Moscow Polytechnic University
          </div>
          <div className="grid-item-header-education-item">
            Computer science,
            CAD integration and programming
          </div>
          <div className="grid-item-header-education-item">
          September 2018 - May 2021
          </div>
        </div>
      </div>
        <div className="grid-container1">
          <div className="grid-item-since">
            i have been <br /><br />
            in the business <br /><br />
            since <br /><br />
            2015
          </div>
          <div className="grid-item-software">
            <div className='grid-item-software-text'>
              S o f t w a r e
            </div>
            <div className='grid-item-software-icons'>
              <img src={cplusplus} alt="ResumeES"/>
              <img src={clang} alt="ResumeES"/>
              <img src={csharp} alt="ResumeES"/>
              <img src={rust} alt="ResumeES"/>
              <img src={android} alt="ResumeES"/>
              <img src={python} alt="ResumeES"/>
              <img src={qt} alt="ResumeES"/>
              <img src={mongodb} alt="ResumeES"/>
              <img src={postgresql} alt="ResumeES"/>
              <img src={nginx} alt="ResumeES"/>
              <img src={docker} alt="ResumeES"/>
              <img src={inventor} alt="ResumeES"/>
              <img src={solid} alt="ResumeES"/>
              <img src={fusion} alt="ResumeES"/>
              <img src={rabbitMQ} alt="ResumeES"/>
              <img src={kafka} alt="ResumeES"/>
            </div>
          </div>
        </div>
      <div className="grid-container2">
        <div className="grid-container-experience-block">
          <div className="grid-container-experience-title">
          Experience
          </div>
          <div className="grid-container-experience-name">
          Lead developer in bt<br />
          </div>
          <div className="grid-container-experience-name">
          Lead developer/ embedder in PromAvto<br />
          </div>
          <div className="grid-container-experience-name">
          Senior developer C++ in 2040 world<br />
          </div>
          <div className="grid-container-experience-name">
          Developer/ embedder in Center for youth innovative creativity<br />
          </div>
        </div>
      </div>
      <div className='grid-container3'>
            <img src={logo_in} alt="Logo" height="80" width="80" />
            https://www.linkedin.com/in/ekaterina-sankova-heisenbug
          </div>
    </div>
  );
}
  /*
export default function ResumePanelES() {
    return(
        <div className='ResumeES'>
          <div className='ResumeES-header'>
            <div className='ResumeES-header-grid'>
            <div className='ResumeES-header-grid-logo'>
                <img src={people1} alt="Logo" />
            </div>
            <div className='ResumeES-header-grid-content'>
                yo
            </div>
            </div>
          </div>
          <div className='ResumeES-body'>
            <div className='ResumeES-body-grid'>
            matherfucker
            <div class="line"></div>
            people
            </div>
          </div>
        </div>
    )
}
*/