import React from 'react';
import { Link } from 'react-router-dom';

import './resume_panel.css'

import people1 from '../../img/people/ekaterina-sankova2.svg';
import people2 from '../../img/people/viktor_tsvetkov2.svg';

export default function ResumePanel() {
    return(
        <div className='Resume'>
            <div className='Resume-header'>

              <div className='Resume-header-icon'>
                <Link to="/resume/ekaterina_sankova">
                  <img src={people1} alt="Logo"
                    width="100"
                    heght="100"/>
                </Link>
                <Link to="/resume/viktor_tsvetkov">
                  <img src={people2} alt="Logo" 
                    width="100"
                    height="100"/>
                  </Link>
              </div>
            </div>
        </div>
    );
}
